import AdminLayout from "../Layout/AdminLayout";
import AdminMoviesPage from "../Page/AdminMoviesPage/AdminMoviesPage";
import AdminUsersPage from "../Page/AdminUsersPage/AdminUsersPage";

export const adminRoute = [
    {
        url: "/admin-users",
        component: <AdminLayout Componet={AdminUsersPage}></AdminLayout>
    },

    {
        url: "/",
        component: <AdminLayout Componet={AdminUsersPage}></AdminLayout>
    },

    {
        url: "/admin-movies",
        component: <AdminLayout Componet={AdminMoviesPage}></AdminLayout>
    },
]