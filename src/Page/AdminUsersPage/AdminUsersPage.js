import { Table } from 'antd'
import React, { useEffect, useState } from 'react'
import { adminServ } from '../../service/adminService';
import { headerColums } from './utils';

export default function AdminUsersPage() {
    const [userList, setUserList] = useState([]);
    useEffect(() => {
        adminServ
            .getUserList()

            .then((res) => {
                setUserList(res.data.content);
                console.log(res)

            })

            .catch((err) => {
                console.log(err);
            })
    }, []);
    console.log(userList.length);
    return (
        <div>
            <Table columns={headerColums} dataSource={userList}></Table>
        </div>
    )
}
